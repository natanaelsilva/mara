//
//  GameHall.h
//  mara
//
//  Created by Natanael Silva on 29/03/14.
//  Copyright (c) 2014 Lambu. All rights reserved.
//

#import "cocos2d.h"
#import "cocos2d-ui.h"
#import "CCScene.h"
@interface GameHall : CCScene

    + (GameHall *)scene;

@end
