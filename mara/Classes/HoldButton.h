//
//  HoldButton.h
//  mara
//
//  Created by Natanael Silva on 18/03/14.
//  Copyright (c) 2014 Lambu. All rights reserved.
//

#import "CCButton.h"

@interface HoldButton : CCButton

@property (nonatomic,copy) void(^blockHold)(id sender);

@property (nonatomic,copy) void(^blockRelease)(id sender);

- (void) setHoldTarget:(id)target selector:(SEL)selector;
- (void) setReleaseTarget:(id)target selector:(SEL)selector;



@end
