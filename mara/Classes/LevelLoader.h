//
//  Level.h
//  mara
//
//  Created by Natanael Silva on 19/03/14.
//  Copyright (c) 2014 Lambu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameLevel.h"

@interface LevelLoader : NSObject

- (void) loadLevelWithGameLevel:(GameLevel*) gameLevel level:(int) level;


@end
