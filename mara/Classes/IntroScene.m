//
//  IntroScene.m
//  mara
//
//  Created by Natanael Silva on 22/01/14.
//  Copyright Lambu 2014. All rights reserved.
//
// -----------------------------------------------------------------------

// Import the interfaces
#import "IntroScene.h"
#import "HelloWorldScene.h"
#import "NewtonScene.h"
#import "GameHall.h"
#import "LevelLoader.h"
#import "CCAnimation.h"

// -----------------------------------------------------------------------
#pragma mark - IntroScene
// -----------------------------------------------------------------------

@implementation IntroScene
{
    CCAnimation *startRunAction;
    CCAnimation *stopRunAction;
    CCAnimation *runAction;
}


#pragma mark - Create & Destroy

+ (IntroScene *)scene
{
	return [[self alloc] init];
}


- (id)init
{
    // Apple recommend assigning self with supers return value
    self = [super init];
    if (!self) return(nil);
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"hero.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"basic_sprites2.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"basic_sprites3.plist"];
    
    // Create a colored background (Dark Grey)
    CCSprite *background = [CCSprite spriteWithImageNamed:@"hall.png"];
    background.position = ccp(self.contentSize.width/2, self.contentSize.height/2);
    [background setScaleX:0.5];
    [background setScaleY:0.5];
    
    [self addChild:background];
    
    CCSprite *title = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"title.png"]];
    title.position = ccp(self.contentSize.width/2, self.contentSize.height*0.6);
    [title setScale:0.5];
    [title setZOrder:5];
    [self addChild:title];
    
    
    CCSprite *hero = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"char1.png"]];
    [hero setScaleX:0.1f];
    [hero setScaleY:0.1f];
    [hero setZOrder:1];
    hero.position = ccp(0.2, 0.7);
    hero.positionType = CCPositionTypeNormalized;
    [self addChild:hero];
    
    
    [self createAnimations];
    
    [hero runAction:[CCActionMoveTo actionWithDuration:20 position:ccp(0.8, 0.2)]];
    
    NSArray *actions = [[NSArray alloc] initWithObjects:
                        [CCActionAnimate actionWithAnimation:startRunAction],
                        [CCActionRepeat actionWithAction:[CCActionAnimate actionWithAnimation:runAction] times:33],
                        [CCActionAnimate actionWithAnimation:stopRunAction], nil];
    
    
    [hero runAction:[CCActionSequence actionWithArray:actions]];
    
    
    // Start button
    CCButton *startButton = [CCButton buttonWithTitle:@""
                                          spriteFrame:[CCSpriteFrame frameWithImageNamed:@"start1.png"]
                               highlightedSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"start2.png"]
                                  disabledSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"start2.png"]];
    startButton.positionType = CCPositionTypeNormalized;
    [startButton setScale:0.2];
    startButton.position = ccp(0.5f, 0.2f);
    [startButton setTarget:self selector:@selector(onStartClicked:)];
    [startButton setZOrder:5];
    [self addChild:startButton];
    
    
    startButton.cascadeOpacityEnabled = YES;
    
    [ startButton runAction:[CCActionRepeatForever
            actionWithAction:[CCActionSequence
                    actionOne:[CCActionFadeOut actionWithDuration:1.0]
                              two:[CCActionFadeIn actionWithDuration:1.0]]]];
    
    //background music
    [[OALSimpleAudio sharedInstance] playBg:@"song_intro.mp3" loop:YES];
	
    // done
	return self;
}

- (void) createAnimations{
    
    NSMutableArray *heroAnimations = [NSMutableArray array];
    
    for(int i = 1; i < 61; ++i)
    {
        CCSpriteFrame *spriteFrame =
        [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: [NSString stringWithFormat:@"char%d.png", i]];
        
        [heroAnimations addObject:spriteFrame];
    }
    
    NSArray *stopArray = [heroAnimations subarrayWithRange:NSMakeRange(0, 5)];
    stopArray = [[[stopArray reverseObjectEnumerator] allObjects] mutableCopy];
    
    startRunAction = [CCAnimation animationWithSpriteFrames:[heroAnimations subarrayWithRange:NSMakeRange(0, 5)] delay:0.03];
    stopRunAction = [CCAnimation animationWithSpriteFrames:stopArray  delay:0.03];
    runAction = [CCAnimation animationWithSpriteFrames:[heroAnimations subarrayWithRange:NSMakeRange(4, 20)] delay:0.03];
}

// -----------------------------------------------------------------------
#pragma mark - Button Callbacks
// -----------------------------------------------------------------------

- (void)onSpinningClicked:(id)sender
{
    // start spinning scene with transition
    [[CCDirector sharedDirector] replaceScene:[HelloWorldScene scene]
                               withTransition:[CCTransition transitionPushWithDirection:CCTransitionDirectionUp duration:0.3f]];
}

- (void)onNewtonClicked:(id)sender
{
    // start newton scene with transition
    // the current scene is pushed, and thus needs popping to be brought back. This is done in the newton scene, when pressing back (upper left corner)
    [[CCDirector sharedDirector] pushScene:[NewtonScene scene]
                            withTransition:[CCTransition transitionPushWithDirection:CCTransitionDirectionLeft duration:1.0f]];
}


- (void)onStartClicked:(id)sender
{
    // start newton scene with transition
    // the current scene is pushed, and thus needs popping to be brought back. This is done in the newton scene, when pressing back (upper left corner)
    
    [[CCDirector sharedDirector] pushScene:[GameHall scene] ];
                            withTransition:[CCTransition transitionPushWithDirection:CCTransitionDirectionLeft duration:1.0f];
}

// -----------------------------------------------------------------------
@end
