//
//  HoldButton.m
//  mara
//
//  Created by Natanael Silva on 18/03/14.
//  Copyright (c) 2014 Lambu. All rights reserved.
//

#import "HoldButton.h"

@implementation HoldButton


- (void) touchEntered:(UITouch *)touch withEvent:(UIEvent *)event
{
    if (!self.enabled)
    {
        return;
    }
    
    [super triggerAction];
    [self schedule:@selector(triggerHoldAction) interval:0.6 repeat:FALSE delay:0.2];
    
    self.highlighted = YES;
}

- (void) touchUpInside:(UITouch *)touch withEvent:(UIEvent *)event
{
    [super setHitAreaExpansion:_originalHitAreaExpansion];
    
    [self unschedule:@selector(triggerHoldAction)];
 
    [self triggerReleaseAction];
    
    self.highlighted = NO;
}


- (void) setHoldTarget:(id)target selector:(SEL)selector
{
    [self setBlockHold:^(id sender) {
        objc_msgSend(target, selector, sender);
	}];
}

- (void) setReleaseTarget:(id)target selector:(SEL)selector
{
    [self setBlockRelease:^(id sender) {
        objc_msgSend(target, selector, sender);
	}];
}


- (void) triggerHoldAction
{
    if (_blockHold)
    {
        _blockHold(self);
    }
}

- (void) triggerReleaseAction
{
    if (_blockRelease)
    {
        _blockRelease(self);
    }
}

@end
