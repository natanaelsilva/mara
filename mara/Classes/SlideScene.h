//
//  SlideScene.h
//  mara
//
//  Created by Natanael Silva on 31/03/14.
//  Copyright (c) 2014 Lambu. All rights reserved.
//

#import "CCScene.h"

@interface SlideScene : CCScene

@property NSMutableArray* slides;

- (id) init;

@end
