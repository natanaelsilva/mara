//
//  StartGameScene.h
//  mara
//
//  Created by Natanael Silva on 24/01/14.
//  Copyright (c) 2014 Lambu. All rights reserved.
//
#import "cocos2d.h"
#import "cocos2d-ui.h"
#import "CCScene.h"
#import "HoldButton.h"

@interface GameLevel : CCScene <CCPhysicsCollisionDelegate>

+ (GameLevel *)sceneWithLevel: (int) level;

@property CCSprite *_hero;

@property int bounces;

@property int shots;

@property int level;

@property int idealNumOfShots;

@property int numOfMonsters;

@property CCPhysicsNode* physicsWorld;

typedef NS_ENUM(NSInteger, HeroStatus) {
    STOPPED,
    WALKING,
    RUNNING,
    STOPPING,
    FIRING,
    CELEBRATING
};

typedef NS_ENUM(NSInteger, HeroDirection) {
    LEFT,
    RIGHT
};

typedef NS_ENUM(NSInteger, FiringState) {
    NORMAL, CHOOSING_TARGET, WAITING_USER
};

- (void) animateMonster:(CCSprite *) monster range:(CGFloat) range duration:(CCTime) duration;

@end

