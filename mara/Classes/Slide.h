//
//  Slide.h
//  mara
//
//  Created by Natanael Silva on 31/03/14.
//  Copyright (c) 2014 Lambu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CCSprite.h"

@interface Slide : NSObject

@property CCSprite *image;
@property NSString *caption;

- (id) initWithSprite: (CCSprite *) image caption: (NSString *) text;

@end
