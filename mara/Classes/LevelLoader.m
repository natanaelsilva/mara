//
//  Level.m
//  mara
//
//  Created by Natanael Silva on 19/03/14.
//  Copyright (c) 2014 Lambu. All rights reserved.
//

#import "LevelLoader.h"

@implementation LevelLoader
{
    GameLevel* gameLevel;
}

- (void) loadLevelWithGameLevel:(GameLevel*) gameLevelp level:(int) level{
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"monster_sheet.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"basic_sprites2.plist"];
    
    self->gameLevel = gameLevelp;
    
    NSString *levelName = [NSString stringWithFormat:@"level%d", level];
    
    NSString* filePath = levelName;
    NSString* fileRoot = [[NSBundle mainBundle]
                          pathForResource:filePath ofType:@"pqp"];
    
    // read everything from text
    NSString* fileContents =
    [NSString stringWithContentsOfFile:fileRoot
                              encoding:NSUTF8StringEncoding error:nil];
    
    // first, separate by new line
    NSArray* allLinedStrings =
    [fileContents componentsSeparatedByCharactersInSet:
     [NSCharacterSet newlineCharacterSet]];
    
    for (int i = 0; i < [allLinedStrings count]; i ++) {
        
        NSString* line = [allLinedStrings objectAtIndex:i];
        
        NSUInteger modifier = [line characterAtIndex:0];
        
        if(modifier != '#'){ //not comment
            
            NSArray* singleStrs =
            [line componentsSeparatedByCharactersInSet:
             [NSCharacterSet characterSetWithCharactersInString:@" "]];
            
            switch (modifier) {
                case 'c':
                    [gameLevel._hero setPosition:ccp(
                        [self fillFloatWithValue:[singleStrs objectAtIndex:1]
                                      relativeTo:gameLevel.contentSize.width],
                        [self fillFloatWithValue:[singleStrs objectAtIndex:2]
                                      relativeTo:gameLevel.contentSize.width])];
                    break;
                case 'o':
                    gameLevel.bounces = [[singleStrs objectAtIndex:1] intValue];
                    break;
                    
                case 's':
                    gameLevel.shots = [[singleStrs objectAtIndex:1] intValue];
                    break;
                    
                case 'i':
                    gameLevel.idealNumOfShots = [[singleStrs objectAtIndex:1] intValue];
                    break;
                    
                case 'g':
                case 'b':
                case 'p':
                case 'f':
                    [self createObjectWithType:modifier
                        width:[[singleStrs objectAtIndex:1] floatValue]
                        height:[[singleStrs objectAtIndex:2] floatValue]
                        x:[self fillFloatWithValue:[singleStrs objectAtIndex:3]
                                        relativeTo:gameLevel.contentSize.width]
                        y:[self fillFloatWithValue:[singleStrs objectAtIndex:4]
                                        relativeTo:gameLevel.contentSize.height]];
                    break;
                case 'm':
                    [self generateMonsterWithWidth:[[singleStrs objectAtIndex:1] floatValue]
                        height:[[singleStrs objectAtIndex:2] floatValue]
                        x:[self fillFloatWithValue:[singleStrs objectAtIndex:3]
                                        relativeTo:gameLevel.contentSize.width]
                        y:[self fillFloatWithValue:[singleStrs objectAtIndex:4]
                                        relativeTo:gameLevel.contentSize.height]
                        range:[[singleStrs objectAtIndex:5] floatValue]
                        duration:[[singleStrs objectAtIndex:6] floatValue]];
                    break;
                    
                default:
                    break;
            }
            
            
        }
    }
    
    
}

- (CGFloat) fillFloatWithValue:(NSString *) textValue relativeTo:(CGFloat) size{
    
    CGFloat value = 0;
    
    if([textValue characterAtIndex:0] == 'r'){
        value = [[textValue substringFromIndex:1] floatValue];
        value *= size;
    }else{
        value = [textValue floatValue];
    }
    
    return value;
}

- (void) createObjectWithType: (NSUInteger) modifier width:(CGFloat) width height:(CGFloat) height x:(CGFloat) x y:(CGFloat) y{
    
    if(modifier == 'g'){
        [self placeGroundWithWidth:width height:height x:x y:y];
    }else if(modifier == 'p'){
        [self placePlatformWithWidth:width height:height x:x y:y];
    }else if(modifier == 'b'){
        [self placeBoxWithWidth:width height:height x:x y:y];
    }else if(modifier == 'f'){
        [self placeFlagWithWidth:width height:height x:x y:y];
    }
    
}

- (void) generateMonsterWithWidth:(CGFloat) width height:(CGFloat)height x:(CGFloat) x y:(CGFloat) y range:(CGFloat) length duration:(CCTime) duration{
    CCSprite *monster = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"monster1"]];
    [monster setScaleX:width];
    [monster setScaleY:height];
    monster.position = CGPointMake(x, y);
    
    CGSize collisionRect = monster.contentSize;
    collisionRect.height -= 10;
    
    monster.physicsBody = [CCPhysicsBody bodyWithRect:(CGRect){ccp(0, 10), collisionRect} cornerRadius:0];
    monster.physicsBody.collisionGroup = @"monsterGroup";
    [monster.physicsBody setCollisionType:@"monsterCollision"];
    monster.physicsBody.allowsRotation = FALSE;
    monster.physicsBody.elasticity = 0;
    
    [gameLevel animateMonster:monster range:length duration:duration];
    
    [gameLevel.physicsWorld addChild:monster];
    
    gameLevel.numOfMonsters++;
}

- (void) placeBoxWithWidth:(CGFloat) width height:(CGFloat) height x:(CGFloat) x y:(CGFloat) y{
    CCSprite *box = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"box.png"]];
    [box setScaleX:0.125f];
    [box setScaleY:0.125f];
    box.position = CGPointMake(x, y);
    box.physicsBody = [CCPhysicsBody bodyWithRect:(CGRect){CGPointZero, box.contentSize} cornerRadius:0];
    box.physicsBody.type = CCPhysicsBodyTypeStatic;
    box.physicsBody.collisionGroup = @"buildingGroup";
    [box setZOrder:2];
    
    [gameLevel.physicsWorld addChild:box];
}

- (void) placeFlagWithWidth:(CGFloat) width height:(CGFloat) height x:(CGFloat) x y:(CGFloat) y{
    
    CCSprite *flag = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"flag.png"]];
    [flag setScaleX:0.4];
    [flag setScaleY:0.4];
    [flag setPosition:ccp(x,y)];
    flag.physicsBody = [CCPhysicsBody bodyWithRect:(CGRect){ccp(0,0),flag.contentSize} cornerRadius:-5];
    flag.physicsBody.collisionGroup = @"buildingGroup";
    [flag.physicsBody setCollisionType:@"flagCollision"];
    flag.physicsBody.type = CCPhysicsBodyTypeStatic;
    [flag setZOrder:2];
    
    [gameLevel.physicsWorld addChild:flag];
}

- (void) placePlatformWithWidth:(CGFloat) width height:(CGFloat) height x:(CGFloat) x y:(CGFloat) y{
    CCSprite *platform = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"platform.png"]];
    platform.position = ccp(x, y);
    [platform setScaleX:width];
    [platform setScaleY:height];
    [platform setZOrder:1];
    
    platform.physicsBody = [CCPhysicsBody bodyWithRect:(CGRect){ccp(0,0),platform.contentSize} cornerRadius:-10];
    platform.physicsBody.collisionGroup = @"buildingGroup";
    [platform.physicsBody setCollisionType:@"buildingCollision"];
    platform.physicsBody.affectedByGravity = FALSE;
    platform.physicsBody.type = CCPhysicsBodyTypeStatic;
    platform.physicsBody.elasticity = 0;
    [gameLevel.physicsWorld addChild:platform];
}

- (void) placeGroundWithWidth:(CGFloat) width height:(CGFloat) height x:(CGFloat) x y:(CGFloat) y{
    CCSprite *floor = [CCSprite spriteWithImageNamed:@"floor.png"];
    floor.position = ccp(x, y);
    [floor setScaleX:width];
    [floor setScaleY:height];
    [floor setZOrder:1];
    
    floor.physicsBody = [CCPhysicsBody bodyWithRect:(CGRect){ccp(0,0),floor.contentSize} cornerRadius:0];
    floor.physicsBody.collisionGroup = @"buildingGroup";
    [floor.physicsBody setCollisionType:@"buildingCollision"];
    floor.physicsBody.affectedByGravity = FALSE;
    floor.physicsBody.type = CCPhysicsBodyTypeStatic;
    floor.physicsBody.elasticity = 0;
    [gameLevel.physicsWorld addChild:floor];
}


@end
