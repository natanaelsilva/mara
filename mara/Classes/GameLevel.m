//
//  StartGameScene.m
//  mara
//
//  Created by Natanael Silva on 24/01/14.
//  Copyright (c) 2014 Lambu. All rights reserved.
//

#import "GameLevel.h"
#import "IntroScene.h"
#import "CCAnimation.h"
#import "HoldButton.h"
#import "LevelLoader.h"


@implementation GameLevel
{
    CCActionJumpTo *_jump;
    
    FiringState firingState; // 0 = normal, 1 = choosing target, 2 = waiting user power 2
    CCParticleSystem *_fire;
    
    //hero animations
    CCAnimation *startRunAction;
    CCAnimation *stopRunAction;
    CCAnimation *runAction;
    CCAnimation *fireAction;
    CCAnimation *winAction;
    
    //monster animatons
    CCAnimation *moveAction;
    CCAnimation *throwAction;
    CCAnimation *dieAction;
    
    HeroStatus heroStatus;
    HeroDirection heroDirection;
    
    HoldButton *rightButton;
    HoldButton *leftButton;
    CCButton *fireButton;
    
    CCButton *soundButton;
    CCButton *musicButton;
    
    
    NSMutableArray *targets;
    
    int MAX_BOUNCES;
    
    BOOL gameOver;
    
    int level;
    
    CCSprite *star1;
    CCSprite *star2;
    CCSprite *star3;
    
    NSString *stars;
}


#pragma mark - Create & Destroy
+ (GameLevel *)sceneWithLevel: (int) level
{
    return [[self alloc] initWithLevel:level];
}

- (id)initWithLevel: (int) level{
    
    self.level = level;

    firingState = NORMAL;
    heroStatus = STOPPED;
    heroDirection = RIGHT;
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"basic_sprites.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"basic_sprites2.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"basic_sprites3.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"hero.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"monster_sheet.plist"];
   
    targets = [[NSMutableArray alloc] init];
    
    stars = @"111";
    
    [self createAnimations];
    
    // Apple recommend assigning self with supers return value
    self = [super init];
    if (!self) return(nil);
    
    // Enable touch handling on scene node
    self.userInteractionEnabled = YES;
    
    //Create the background
    CCSprite *background = [CCSprite spriteWithImageNamed:@"bg.png"];
    background.position = ccp(self.contentSize.width/2, self.contentSize.height/2);
    [background setScaleX:0.5];
    [background setScaleY:0.5];
    
    [self addChild:background];
    
    // Instantiating the hero sprite
    self._hero = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"char1.png"]];
    [self._hero setScaleX:0.2f];
    [self._hero setScaleY:0.2f];
    [self._hero setZOrder:3];

    //add the four buttons
    [self setupButtons];
    
    //background music
    [[OALSimpleAudio sharedInstance] stopBg];
    [[OALSimpleAudio sharedInstance] playBg:@"song_soundtrack.mp3" loop:YES];
    [OALSimpleAudio sharedInstance].bgVolume = 0.8;
    
    //setting physics world properties
    self.physicsWorld = [CCPhysicsNode node];
    self.physicsWorld.gravity = ccp(0,-200);
    self.physicsWorld.debugDraw = FALSE;
    self.physicsWorld.collisionDelegate = self;
    [self addChild:self.physicsWorld];
    
    CGSize collisionRectangle = self._hero.contentSize;
    collisionRectangle.height *= 0.8;
    collisionRectangle.width *= 0.6;
    
    self._hero.physicsBody = [CCPhysicsBody bodyWithRect:(CGRect){ccp(0,15),collisionRectangle} cornerRadius:-10];
    self._hero.physicsBody.collisionGroup = @"playerGroup";
    [self._hero.physicsBody setCollisionType:@"charCollision"];
    self._hero.physicsBody.density = 1;
    [self.physicsWorld addChild:self._hero];

    [[LevelLoader alloc] loadLevelWithGameLevel:self level:level];
    
    MAX_BOUNCES = self.bounces;
    
    [self setupLegend];
    
    // done
	return self;
}

- (void) setupLegend{
    star1 = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"star_full.png"]];
    [star1 setScaleX:0.5];
    [star1 setScaleY:0.5];
    [star1 setPosition:ccp(self.contentSize.width*0.13, self.contentSize.height*0.94)];
    [star1 setZOrder:5];
    [self addChild:star1];
    
    star2 = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"star_full.png"]];
    [star2 setScaleX:0.5];
    [star2 setScaleY:0.5];
    [star2 setPosition:ccp(self.contentSize.width*0.18, self.contentSize.height*0.94)];
    [star2 setZOrder:5];
    [self addChild:star2];
    
    star3 = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"star_full.png"]];
    [star3 setScaleX:0.5];
    [star3 setScaleY:0.5];
    [star3 setPosition:ccp(self.contentSize.width*0.23, self.contentSize.height*0.94)];
    [star3 setZOrder:5];
    [self addChild:star3];
    
}

- (void) setupButtons{
    //add the four buttons
    leftButton = [HoldButton buttonWithTitle:@""
                                 spriteFrame:[CCSpriteFrame frameWithImageNamed:@"button_move_trans.png"]
                      highlightedSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"button_move.png"]
                         disabledSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"button_move.png"]];
    leftButton.positionType = CCPositionTypeNormalized;
    leftButton.position = ccp(0.05f, 0.5f);
    [leftButton setOpacity:0.2f];
    [leftButton setTarget:self selector:@selector(startRunLeft:)];
    [leftButton setHoldTarget:self selector:@selector(run:)];
    [leftButton setReleaseTarget:self selector:@selector(stop:)];
    [leftButton setZOrder:5];
    [self addChild:leftButton];
    
    rightButton = [HoldButton buttonWithTitle:@""
                                  spriteFrame:[CCSpriteFrame frameWithImageNamed:@"button_move_trans.png"]
                       highlightedSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"button_move.png"]
                          disabledSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"button_move.png"]];
    rightButton.positionType = CCPositionTypeNormalized;
    rightButton.position = ccp(0.95f, 0.5f);
    rightButton.scaleX = -1; //flip the button
    
    [rightButton setTarget:self selector:@selector(startRunRight:)];
    [rightButton setHoldTarget:self selector:@selector(run:)];
    [rightButton setReleaseTarget:self selector:@selector(stop:)];
    [rightButton setZOrder:5];
    [self addChild:rightButton];

    fireButton = [CCButton buttonWithTitle:@""
                                         spriteFrame:[CCSpriteFrame frameWithImageNamed:@"button_fire_trans.png"]
                              highlightedSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"button_fire.png"]
                                 disabledSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"button_fire.png"]];
    fireButton.positionType = CCPositionTypeNormalized;
    fireButton.position = ccp(0.5f, 0.07f);
    [fireButton setScaleX:0.7];
    [fireButton setScaleY:0.7];
    [fireButton setTarget:self selector:@selector(onFireButtonClicked:)];
    [fireButton setZOrder:5];
    [self addChild:fireButton];
    
    // Create a back button
    CCButton *backButton = [CCButton buttonWithTitle:@""
                                         spriteFrame:[CCSpriteFrame frameWithImageNamed:@"back1.png"]
                              highlightedSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"back2.png"]
                                 disabledSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"back2.png"]];
    [backButton setScale:0.25];
    backButton.positionType = CCPositionTypeNormalized;
    backButton.position = ccp(0.93f, 0.95f); // Top Right of screen
    [backButton setTarget:self selector:@selector(onBackClicked:)];
    [backButton setZOrder:6];
    [self addChild:backButton];
    
    soundButton = [CCButton buttonWithTitle:@""
                                          spriteFrame:[CCSpriteFrame frameWithImageNamed:@"sound-icon.png"]];
    [soundButton setScale:0.25];
    soundButton.positionType = CCPositionTypeNormalized;
    soundButton.position = ccp(0.83f, 0.95f); // Top Right of screen
    [soundButton setTarget:self selector:@selector(muteSoundEffects:)];
    soundButton.cascadeOpacityEnabled = YES;
    
    [self addChild:soundButton];
    
    musicButton = [CCButton buttonWithTitle:@""
                                          spriteFrame:[CCSpriteFrame frameWithImageNamed:@"music-icon.png"]];
    [musicButton setScale:0.25];
    musicButton.positionType = CCPositionTypeNormalized;
    musicButton.position = ccp(0.77f, 0.95f); // Top Right of screen
    [musicButton setTarget:self selector:@selector(muteMusic:)];
    musicButton.cascadeOpacityEnabled = YES;
    [self addChild:musicButton];
    
    if([OALSimpleAudio sharedInstance].effectsMuted){
        soundButton.opacity = 0.3;
    }
    
    if([OALSimpleAudio sharedInstance].bgMuted){
        musicButton.opacity = 0.3;
    }

}

// -----------------------------------------------------------------------

- (void)dealloc
{
    // clean up code goes here
}

// -----------------------------------------------------------------------
#pragma mark - Enter & Exit
// -----------------------------------------------------------------------

- (void)onEnter
{
    // always call super onEnter first
    [super onEnter];
    //    [self schedule:@selector(addMonster:) interval:3.5];
    
    
    // In pre-v3, touch enable and scheduleUpdate was called here
    // In v3, touch is enabled by setting userInterActionEnabled for the individual nodes
    // Pr frame update is automatically enabled, if update is overridden
    
      [self setMultipleTouchEnabled:true];
    
}

// -----------------------------------------------------------------------

- (void)onExit
{
    // always call super onExit last
    [super onExit];
}

// -----------------------------------------------------------------------
#pragma mark - Touch Handler
// -----------------------------------------------------------------------

-(void) touchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    
    if(firingState == CHOOSING_TARGET){
        [self fire:touch];
    }else if(firingState == WAITING_USER){
        [self firePower2];
    }
}

- (void) firePower2{
    firingState = NORMAL;
}

- (void) fire:(UITouch *)touch{
    
    self.bounces--;
    [targets addObject:[NSValue valueWithCGPoint:[touch locationInNode:self] ]];
    
    
    if(self.bounces == 0){
        
        CCSprite *projectile = [self createProjectile];
        
        NSMutableArray *projectileMoves = [[NSMutableArray alloc] init];
        
        int length = [targets count];
        
        float distance = 0;
        
        float speed = 100*MAX_BOUNCES;
        
        for(int i = 0; i < length; i++){
            
            CGPoint touchLocation = [[targets objectAtIndex:i] CGPointValue];
            CGPoint lastPoint;
            
            
            CGPoint offset;
            if(i > 0){
                offset = ccpSub(touchLocation, [[targets objectAtIndex:(i -1)] CGPointValue]);
                lastPoint = [[targets objectAtIndex:(i-1)] CGPointValue];
            }else{
                offset = ccpSub(touchLocation, self._hero.position);
                lastPoint = self._hero.position;
            }
            
            int targetX, targetY;
            
            if(i == length - 1){ //the last touch, fire without an end
                
                int factor = self.contentSize.width/offset.x + 1; //see how much does fit in the screen and add one to make sure it is out of sight
                
                // 2
                
                //if it was touched before the hero
                if(touchLocation.x < lastPoint.x){
                    targetX = lastPoint.x - offset.x * factor;
                    targetY = lastPoint.y - offset.y * factor;
                    
                    if(heroDirection == RIGHT){
                        [self flipHero];
                    }
                    
                }else{
                    targetX = offset.x * factor + lastPoint.x;
                    targetY   = offset.y * factor + lastPoint.y;
                    
                    if(heroDirection == LEFT){
                        [self flipHero];
                    }
                }
                
                CGPoint targetPosition = ccp(targetX,targetY);
            
                distance += ccpDistance(lastPoint, targetPosition);
                
                [projectileMoves addObject: [CCActionMoveTo actionWithDuration:distance/speed position:targetPosition]];
                [projectileMoves addObject: [CCActionRemove action]];
                
                self.shots--;
                
                if(self.shots == 0){
                    [self removeChild:fireButton];
                }
                
            }else{
                
                distance += ccpDistance(lastPoint, ccpAdd(offset, lastPoint));
                [projectileMoves addObject:[CCActionMoveTo actionWithDuration:distance/speed position:ccpAdd(offset, lastPoint)]];
            }
        }
        
        firingState = NORMAL;
        self.bounces = MAX_BOUNCES;
        [targets removeAllObjects];
        
        [projectile runAction:[CCActionSequence actionWithArray:projectileMoves]];
        [[OALSimpleAudio sharedInstance] playEffect:@"fireball.mp3" loop:NO];
    }
    
    //play sound
    //    [[OALSimpleAudio sharedInstance] playEffect:@"pew-pew-lei.caf"];
}

- (CCSprite *) createProjectile{
    
    CCSprite *projectile = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"fireball.png"]];
    [projectile setScaleX:0.3f];
    [projectile setScaleY:0.3f];
    
    projectile.position = self._hero.position;
    
    projectile.physicsBody = [CCPhysicsBody bodyWithCircleOfRadius:projectile.contentSize.width/2.0f andCenter:projectile.anchorPointInPoints];
    projectile.physicsBody.collisionGroup = @"playerGroup";
    [projectile.physicsBody setCollisionType:@"projectileCollision"];
    projectile.physicsBody.affectedByGravity =  FALSE;
    [self.physicsWorld addChild:projectile];
    
    // set the fire ************
    [self setObjectOnFire:projectile];
    
    return projectile;
}


- (void)setObjectOnFire:(CCSprite *)projectile{
    // create fire
    _fire = [CCParticleSystem particleWithFile:@"fire.plist"];
    _fire.particlePositionType = CCParticleSystemPositionTypeFree;
    
    // place the fire effect in upper half of sphere (looks better)
    _fire.position = ccp(projectile.contentSize.width/2, projectile.contentSize.height * 0.35f);
    
    // ----------
    // Issue #484
    // There is a bug in particle systems, that prevents free particles from being scaled. In stead the properties must be scaled.
    float particleScale = 3.0f * [CCDirector sharedDirector].viewSize.width / 1000;
    _fire.startSize *= particleScale;
    _fire.startSizeVar *= particleScale;
    _fire.endSize *= particleScale;
    _fire.endSizeVar *= particleScale;
    _fire.gravity = ccpMult(_fire.gravity, particleScale/8);
    _fire.posVar = ccpMult(_fire.posVar, particleScale/2);
    
    // this produces weird results
    // _fire.scale = particleScale;
    // ----------
    
    [projectile addChild:_fire];
}

// -----------------------------------------------------------------------
#pragma mark - collisions
// -----------------------------------------------------------------------

- (BOOL)ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair monsterCollision:(CCNode *)monster projectileCollision:(CCNode *)projectile {
    
    //[projectile removeFromParent];
    [monster stopAllActions];
    
    [self setObjectOnFire:monster];
    
    CCActionAnimate *dieAnimationAction = [CCActionAnimate actionWithAnimation:dieAction];
    
    CCActionFadeOut *fadeAction = [[CCActionFadeOut alloc] initWithDuration:0.3f];
    CCAction *actionRemove = [CCActionRemove action];
    
    NSArray *animations = [[NSArray alloc] initWithObjects:dieAnimationAction, fadeAction, actionRemove, nil];
    
    [monster runAction:[CCActionSequence actionWithArray:animations]];
    [[OALSimpleAudio sharedInstance] playEffect:@"fire_loop.mp3" loop:NO];
    [[OALSimpleAudio sharedInstance] playEffect:@"monster_death.mp3" loop:NO];
    
    self.numOfMonsters--;
    
    //play hit sound
    //    [[OALSimpleAudio sharedInstance] playEffect:@"pew-pew-lei.caf"];
    
    [self checkStars];
    
    return YES;
}

- (BOOL)ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair buildingCollision:(CCNode *)monster projectileCollision:(CCNode *)projectile {
    
    [projectile removeFromParent];
    
    [self checkStars];
    
    //play hit sound
    //    [[OALSimpleAudio sharedInstance] playEffect:@"pew-pew-lei.caf"];
    
    return YES;
}

- (BOOL)ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair charCollision:(CCNode *)hero flagCollision:(CCNode *)flag {
    [self win];
    
    //play hit sound
    //    [[OALSimpleAudio sharedInstance] playEffect:@"pew-pew-lei.caf"];
    
    return NO;
}

- (BOOL)ccPhysicsCollisionBegin:(CCPhysicsCollisionPair *)pair charCollision:(CCNode *)hero monsterCollision:(CCNode *)monster {
    
    [self._hero runAction:[[CCActionSequence alloc] initOne:[[CCActionFadeOut alloc] initWithDuration:0.3f] two:[CCActionRemove action]]];
    [[OALSimpleAudio sharedInstance] playEffect:@"hero_death.mp3" loop:NO];
    
    [self gameOver];
    
    stars = @"000";
    
    [self updateStars];
    
    return YES;
}


// -----------------------------------------------------------------------
#pragma mark - Button Callbacks
// -----------------------------------------------------------------------

- (void)touchEnded:(UITouch *)touch withEvent:(UIEvent *)event{
    
    //    if([touch previousLocationInView:[touch view]].y > [touch locationInView:[touch view]].y){
    //        _jump = [[CCActionJumpTo alloc] initWithDuration:0.55f position:ccp(_hero.position.x,_hero.position.y)
    //                                                  height:120.0f jumps:1];
    //        [_hero runAction:_jump];
    //    }
}


- (void)onBackClicked:(id)sender
{
    // back to intro scene with transition
    [[CCDirector sharedDirector] popSceneWithTransition:[CCTransition transitionFadeWithDuration:1.0f]];
    
    [[OALSimpleAudio sharedInstance] stopBg];
    [[OALSimpleAudio sharedInstance] playBg:@"song_intro.mp3" loop:YES];
}

- (void)restartLevel:(id)sender
{
    // pop previous scene back in place
    [[CCDirector sharedDirector] replaceScene:[GameLevel sceneWithLevel:self.level] withTransition:[CCTransition transitionPushWithDirection:CCTransitionDirectionRight duration:1.0f]];
}

- (void)onShieldButtonClicked:(id)sender
{
    firingState = WAITING_USER;
    //change sprite of the button
}

- (void)onFireButtonClicked:(id)sender
{
    firingState = CHOOSING_TARGET;
    //change sprite of the button
}


- (void) createAnimations{
    
    NSMutableArray *heroAnimations = [NSMutableArray array];
    NSMutableArray *monsterAnimations = [NSMutableArray array];
    
    for(int i = 1; i < 61; ++i)
    {
        CCSpriteFrame *spriteFrame =
            [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: [NSString stringWithFormat:@"char%d.png", i]];
        
        [heroAnimations addObject:spriteFrame];
    }
    
    NSArray *stopArray = [heroAnimations subarrayWithRange:NSMakeRange(0, 5)];
    stopArray = [[[stopArray reverseObjectEnumerator] allObjects] mutableCopy];
    
    startRunAction = [CCAnimation animationWithSpriteFrames:[heroAnimations subarrayWithRange:NSMakeRange(0, 5)] delay:0.03];
    stopRunAction = [CCAnimation animationWithSpriteFrames:stopArray  delay:0.03];
    runAction = [CCAnimation animationWithSpriteFrames:[heroAnimations subarrayWithRange:NSMakeRange(4, 20)] delay:0.03];
    fireAction = [CCAnimation animationWithSpriteFrames:[heroAnimations subarrayWithRange:NSMakeRange(25, 15)] delay:0.03];
    winAction = [CCAnimation animationWithSpriteFrames:[heroAnimations subarrayWithRange:NSMakeRange(40, 20)] delay:0.04];
    
    for(int i = 1; i < 55; ++i)
    {
        CCSpriteFrame *spriteFrame =
            [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: [NSString stringWithFormat:@"monster%d", i]];
        
        [monsterAnimations addObject:spriteFrame];
    }
    
    
    moveAction = [CCAnimation animationWithSpriteFrames:[monsterAnimations subarrayWithRange:NSMakeRange(0, 20)] delay:0.1];
    throwAction = [CCAnimation animationWithSpriteFrames:[monsterAnimations subarrayWithRange:NSMakeRange(20, 15)] delay:0.03];
    dieAction = [CCAnimation animationWithSpriteFrames:[monsterAnimations subarrayWithRange:NSMakeRange(35, 19)] delay:0.1];
}

- (void) animateMonster:(CCSprite *) monster range:(CGFloat) range duration:(CCTime) duration{
    CGPoint position1 = monster.position;
    CGPoint position2 = monster.position;
    position2.x -= range;
    
    
    
    CCActionMoveTo *walkLeft = [CCActionMoveTo actionWithDuration:duration position:position2];
    CCActionMoveTo *walkRight = [CCActionMoveTo actionWithDuration:duration position:position1];
    CCActionSequence *zigzag = [CCActionSequence actionOne:walkLeft two:walkRight];
    
    CCActionAnimate *animation = [CCActionAnimate actionWithAnimation:moveAction];
    
    [monster runAction:[CCActionRepeatForever actionWithAction:zigzag]];
    [monster runAction:[CCActionRepeatForever actionWithAction:animation]];
}

- (void)muteSoundEffects:(id)sender{
    [OALSimpleAudio sharedInstance].effectsMuted = ![OALSimpleAudio sharedInstance].effectsMuted;
    
    if([OALSimpleAudio sharedInstance].effectsMuted == YES){
        [soundButton setOpacity:0.3];
    }else{
        [soundButton setOpacity:1.0];
    }
}

- (void)muteMusic:(id)sender{
    [OALSimpleAudio sharedInstance].bgMuted = ![OALSimpleAudio sharedInstance].bgMuted;
    
    if([OALSimpleAudio sharedInstance].bgMuted == YES){
        [musicButton setOpacity:0.3];
    }else{
        [musicButton setOpacity:1.0];
    }
}

#pragma mark Hero movement

- (void)startRunRight:(id)sender
{
    [self turnRight];
    CGPoint nextPoint = self._hero.position;
    nextPoint.x = nextPoint.x + 3;
    CCActionMoveTo *move = [CCActionMoveTo actionWithDuration:0.3 position:nextPoint];
    CCActionAnimate *animationAction = [CCActionAnimate actionWithAnimation:startRunAction];
    
    [self._hero runAction:move];
    [self._hero runAction:animationAction];
}

- (void)startRunLeft:(id)sender
{
    [self turnLeft];
    CGPoint nextPoint = self._hero.position;
    nextPoint.x = nextPoint.x - 3;
    CCActionMoveTo *move = [CCActionMoveTo actionWithDuration:0.3 position:nextPoint];
    CCActionAnimate *animationAction = [CCActionAnimate actionWithAnimation:startRunAction];
    
    [self._hero runAction:move];
    [self._hero runAction:animationAction];
}

- (void)run:(id)sender
{
    CGPoint nextPoint = self._hero.position;
    
    if(heroDirection == RIGHT){
        nextPoint.x = nextPoint.x + 20;
    }else{
        nextPoint.x = nextPoint.x - 20;
    }
    
    CCActionMoveTo *move = [CCActionMoveTo actionWithDuration:0.6 position:nextPoint];
    CCActionAnimate *animationAction = [CCActionAnimate actionWithAnimation:runAction];
    
    [self._hero runAction:move];
    [self._hero runAction:animationAction];
}

- (void)stop:(id)sender
{
    CGPoint nextPoint = self._hero.position;
    
    if(heroDirection == RIGHT){
        nextPoint.x = nextPoint.x + 1;
    }else{
        nextPoint.x = nextPoint.x - 1;
    }
    
    CCActionMoveTo *move = [CCActionMoveTo actionWithDuration:0.3 position:nextPoint];
    CCActionAnimate *animationAction = [CCActionAnimate actionWithAnimation:stopRunAction];
    
    [self._hero runAction:move];
    [self._hero runAction:animationAction];
}

- (void) turnRight{
    if(heroDirection == LEFT){
        heroDirection = RIGHT;
        self._hero.flipX = !self._hero.flipX;
    }
}

- (void) turnLeft{
    if(heroDirection == RIGHT){
        heroDirection = LEFT;
        self._hero.flipX = !self._hero.flipX;
    }
}

- (void) flipHero{
    if(heroDirection == LEFT){
        heroDirection = RIGHT;
    }else{
        heroDirection = LEFT;
    }
    
    self._hero.flipX = !self._hero.flipX;
}

#pragma mark Game Engine

- (void) checkStars{
    if(self.shots < self.idealNumOfShots){
        stars = [stars stringByReplacingCharactersInRange:NSMakeRange(2, 1) withString:@"0"];
    }
    
    if(self.shots == 0 && self.numOfMonsters > 0){
        stars = [stars stringByReplacingCharactersInRange:NSMakeRange(1, 1) withString:@"0"];
    }
    
    [self updateStars];
}

- (void) updateStars{
    
    if([stars  isEqual: @"111"] ){
        [star1 setSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"star_full.png"]];
        [star2 setSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"star_full.png"]];
        [star3 setSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"star_full.png"]];
        
    }else if([stars  isEqual: @"101"] || [stars  isEqual: @"110"]){
        [star1 setSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"star_full.png"]];
        [star2 setSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"star_full.png"]];
        [star3 setSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"star_empty.png"]];
        
    }else if([stars  isEqual: @"100"]){
        [star1 setSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"star_full.png"]];
        [star2 setSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"star_empty.png"]];
        [star3 setSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"star_empty.png"]];
        
    }else if([stars  isEqual: @"000"]){
        [star1 setSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"star_empty.png"]];
        [star2 setSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"star_empty.png"]];
        [star3 setSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"star_empty.png"]];
    }
}

- (void) gameOver{
    
    CCSprite *frame = [CCSprite spriteWithImageNamed:@"frame2.png"];
    [frame setScaleX:0.01];
    [frame setScaleY:0.01];
    [frame setPosition:ccp(self.contentSize.width*0.5, self.contentSize.height*0.55)];
    [frame setZOrder:4];
    [frame setOpacity:0.7];
    
    [self addChild:frame];
    
    CCActionScaleTo *scaleAnimation = [CCActionScaleTo actionWithDuration:1.5 scale:0.7];
    [frame runAction:scaleAnimation];
    
    gameOver = true;
    [self hideButtons];
    
    CCLabelTTF *label = [CCLabelTTF labelWithString:@"You lost!" fontName:@"Chalkduster" fontSize:17.0f];
    label.position = ccp(self.contentSize.width*0.5, self.contentSize.height*0.7);
    [label setZOrder:5];
    [label setOpacity:0];
    
    [label runAction:[CCActionSequence actionOne:[CCActionInterval actionWithDuration:1.5] two:[CCActionFadeIn actionWithDuration:1.5]]];
    [[OALSimpleAudio sharedInstance] stopBg];
    [[OALSimpleAudio sharedInstance] playEffect:@"lose.mp3" loop:NO];
    
    [self addChild:label];
}

- (void) win {
    
    if(!gameOver){
        [self frameAnimation];
        
        gameOver = true;
        
        [self._hero stopAllActions];
        
        CCActionAnimate *animationAction = [CCActionAnimate actionWithAnimation:winAction];
        [self._hero runAction:[CCActionRepeatForever actionWithAction:animationAction]];
        [[OALSimpleAudio sharedInstance] stopBg];
        [[OALSimpleAudio sharedInstance] playEffect:@"guitar_riff.mp3" loop:NO];
        
        [self hideButtons];
    }
    
}

- (void) frameAnimation{
    CCSprite *frame = [CCSprite spriteWithImageNamed:@"frame.png"];
    
    [frame setScaleX:0.01];
    [frame setScaleY:0.01];
    [frame setPosition:ccp(self.contentSize.width*0.5, self.contentSize.height*0.55)];
    [frame setZOrder:4];
    [frame setOpacity:0.7];
    
    [self addChild:frame];
    
    [frame runAction:[CCActionScaleTo actionWithDuration:1.5 scale:0.7]];
    
    [star1 runAction:[CCActionMoveTo actionWithDuration:1.5
                        position:ccp(self.contentSize.width*0.45,self.contentSize.height*0.5)]];
    [star1 runAction:[CCActionScaleTo actionWithDuration:1.5 scale:0.8]];
    
    [star2 runAction:[CCActionMoveTo actionWithDuration:1.5
                        position:ccp(self.contentSize.width*0.52,self.contentSize.height*0.5)]];
    [star2 runAction:[CCActionScaleTo actionWithDuration:1.5 scale:0.8]];

    [star3 runAction:[CCActionMoveTo actionWithDuration:1.5
                        position:ccp(self.contentSize.width*0.59,self.contentSize.height*0.5)]];
    [star3 runAction:[CCActionScaleTo actionWithDuration:1.5 scale:0.8]];
    
    CCLabelTTF *label = [CCLabelTTF labelWithString:@"You made it to\n the next level!" fontName:@"Chalkduster" fontSize:17.0f];
    label.position = ccp(self.contentSize.width*0.5, self.contentSize.height*0.7);
    [label setZOrder:5];
    [label setOpacity:0];
    
    CCButton *updateButton = [CCButton buttonWithTitle:@""
                                           spriteFrame:[CCSpriteFrame frameWithImageNamed:@"update1.png"]
                                highlightedSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"update2.png"]
                                   disabledSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"update2.png"]];
    [updateButton setScale:0.5];
    updateButton.positionType = CCPositionTypeNormalized;
    updateButton.position = ccp(0.28f, 0.32f);
    [updateButton setTarget:self selector:@selector(restartLevel:)];
    [updateButton setZOrder:6];
    updateButton.cascadeOpacityEnabled = YES;
    [updateButton setOpacity:0];
    
    CCButton *arrowButton = [CCButton buttonWithTitle:@""
                                           spriteFrame:[CCSpriteFrame frameWithImageNamed:@"arrow1.png"]
                                highlightedSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"arrow2.png"]
                                   disabledSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"arrow2.png"]];
    [arrowButton setScale:0.5];
    arrowButton.positionType = CCPositionTypeNormalized;
    arrowButton.position = ccp(0.72f, 0.32f);
    [arrowButton setTarget:self selector:@selector(onBackClicked:)];
    [arrowButton setZOrder:6];
    arrowButton.cascadeOpacityEnabled = YES;
    [arrowButton setOpacity:0];
    
    
    [label runAction:[CCActionSequence actionOne:[CCActionInterval actionWithDuration:1.5] two:[CCActionFadeIn actionWithDuration:1.5]]];
    [updateButton runAction:[CCActionSequence actionOne:[CCActionInterval actionWithDuration:1.5] two:[CCActionFadeIn actionWithDuration:1.5]]];
    [arrowButton runAction:[CCActionSequence actionOne:[CCActionInterval actionWithDuration:1.5] two:[CCActionFadeIn actionWithDuration:1.5]]];
    
    [self addChild:label];
    [self addChild:updateButton];
    [self addChild:arrowButton];
}

- (void) hideButtons{
    [self removeChild:rightButton];
    [self removeChild:leftButton];
    
    if(self.shots > 0 ){
        [self removeChild:fireButton];
    }
}

@end
