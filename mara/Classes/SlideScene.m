//
//  SlideScene.m
//  mara
//
//  Created by Natanael Silva on 31/03/14.
//  Copyright (c) 2014 Lambu. All rights reserved.
//

#import "SlideScene.h"
#import "Slide.h"

@implementation SlideScene


- (id) init{
 
    
    self.slides = [[NSMutableArray alloc ] initWithObjects:
                    [self cslide:@"hall.bg" caption:@"BLA BLA BLA MOTHERFUCKER 1"],
                    [self cslide:@"hall.bg" caption:@"BLA BLA BLA MOTHERFUCKER 2"],
                    [self cslide:@"hall.bg" caption:@"BLA BLA BLA MOTHERFUCKER 3"],
                    [self cslide:@"hall.bg" caption:@"BLA BLA BLA MOTHERFUCKER 4"],
                    [self cslide:@"hall.bg" caption:@"BLA BLA BLA MOTHERFUCKER 5"],
                   nil];
    
    [self startSlideShow];
    
    return self;
}


- (Slide *) cslide: (NSString *) spriteName caption:(NSString *) text{
    return [[Slide alloc] initWithSprite:[CCSprite spriteWithImageNamed:spriteName] caption:text];
}

- (void) startSlideShow {
    
    
    
    
}

@end
