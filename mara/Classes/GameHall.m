//
//  GameHall.m
//  mara
//
//  Created by Natanael Silva on 29/03/14.
//  Copyright (c) 2014 Lambu. All rights reserved.
//

#import "GameHall.h"
#import "GameLevel.h"

@implementation GameHall

{
    CCButton *soundButton;
    CCButton *musicButton;
    
}

#pragma mark - Create & Destroy
+ (GameHall *)scene
{
    return [[self alloc] init];
}

- (id)init{
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"flames.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"basic_sprites2.plist"];
    // Apple recommend assigning self with supers return value
    self = [super init];
    if (!self) return(nil);
    
    // Enable touch handling on scene node
    self.userInteractionEnabled = YES;
    
    //Create the background
    CCSprite *background = [CCSprite spriteWithImageNamed:@"hall.png"];
    background.position = ccp(self.contentSize.width/2, self.contentSize.height/2);
    [background setScaleX:0.5];
    [background setScaleY:0.5];
    
    [self addChild:background];
    
    //add the level buttons
    [self setupButtons];
    
    //background music
    //    [[OALSimpleAudio sharedInstance] playBg:@"background-music-aac.caf" loop:YES];
	return self;
}

- (void) setupButtons{
    //add the four buttons
    
        CCButton *levelButton = [CCButton buttonWithTitle:@""
                                              spriteFrame:[CCSpriteFrame frameWithImageNamed:[NSString stringWithFormat:@"flame%d.png", 1]]
                                   highlightedSpriteFrame:[CCSpriteFrame frameWithImageNamed:[NSString stringWithFormat:@"flame%d.png", 1+5]]
                                      disabledSpriteFrame:[CCSpriteFrame frameWithImageNamed:[NSString stringWithFormat:@"flame%d.png", 1+10]]];
        levelButton.positionType = CCPositionTypeNormalized;
        levelButton.position = ccp(0.2f, 0.12f);
        [levelButton setScale:0.25];
        [levelButton setOpacity:0.2f];
        [levelButton setTarget:self selector:@selector(openLevel1:)];
        [levelButton setZOrder:5];
        [self addChild:levelButton];
    
        CCButton *levelButton2 = [CCButton buttonWithTitle:@""
                                          spriteFrame:[CCSpriteFrame frameWithImageNamed:[NSString stringWithFormat:@"flame%d.png", 2]]
                               highlightedSpriteFrame:[CCSpriteFrame frameWithImageNamed:[NSString stringWithFormat:@"flame%d.png", 2+5]]
                                  disabledSpriteFrame:[CCSpriteFrame frameWithImageNamed:[NSString stringWithFormat:@"flame%d.png", 2+10]]];
        levelButton2.positionType = CCPositionTypeNormalized;
        levelButton2.position = ccp(0.35f, 0.12f);
        [levelButton2 setScale:0.25];
        [levelButton2 setOpacity:0.2f];
        [levelButton2 setTarget:self selector:@selector(openLevel2:)];
        [levelButton2 setZOrder:5];
        [self addChild:levelButton2];
    
        CCButton *levelButton3 = [CCButton buttonWithTitle:@""
                                           spriteFrame:[CCSpriteFrame frameWithImageNamed:[NSString stringWithFormat:@"flame%d.png", 3]]
                                highlightedSpriteFrame:[CCSpriteFrame frameWithImageNamed:[NSString stringWithFormat:@"flame%d.png", 3+5]]
                                   disabledSpriteFrame:[CCSpriteFrame frameWithImageNamed:[NSString stringWithFormat:@"flame%d.png", 3+10]]];
        levelButton3.positionType = CCPositionTypeNormalized;
        levelButton3.position = ccp(0.5f, 0.22f);
        [levelButton3 setScale:0.25];
        [levelButton3 setOpacity:0.2f];
        [levelButton3 setTarget:self selector:@selector(openLevel3:)];
        [levelButton3 setZOrder:5];
        [self addChild:levelButton3];

        CCButton *levelButton4 = [CCButton buttonWithTitle:@""
                                           spriteFrame:[CCSpriteFrame frameWithImageNamed:[NSString stringWithFormat:@"flame%d.png", 4]]
                                highlightedSpriteFrame:[CCSpriteFrame frameWithImageNamed:[NSString stringWithFormat:@"flame%d.png", 4+5]]
                                   disabledSpriteFrame:[CCSpriteFrame frameWithImageNamed:[NSString stringWithFormat:@"flame%d.png", 4+10]]];
        levelButton4.positionType = CCPositionTypeNormalized;
        levelButton4.position = ccp(0.73f, 0.22f);
        [levelButton4 setScale:0.25];
        [levelButton4 setOpacity:0.2f];
        [levelButton4 setTarget:self selector:@selector(openLevel4:)];
        [levelButton4 setZOrder:5];
        [self addChild:levelButton4];
    
        CCButton *levelButton5 = [CCButton buttonWithTitle:@""
                                           spriteFrame:[CCSpriteFrame frameWithImageNamed:[NSString stringWithFormat:@"flame%d.png", 5]]
                                highlightedSpriteFrame:[CCSpriteFrame frameWithImageNamed:[NSString stringWithFormat:@"flame%d.png", 5+5]]
                                   disabledSpriteFrame:[CCSpriteFrame frameWithImageNamed:[NSString stringWithFormat:@"flame%d.png", 5+10]]];
        levelButton5.positionType = CCPositionTypeNormalized;
        levelButton5.position = ccp(0.9f, 0.36f);
        [levelButton5 setScale:0.25];
        [levelButton5 setOpacity:0.2f];
        [levelButton5 setTarget:self selector:@selector(openLevel5:)];
        [levelButton5 setZOrder:5];
        [self addChild:levelButton5];
    
    // Create a back button
    CCButton *backButton = [CCButton buttonWithTitle:@""
                                         spriteFrame:[CCSpriteFrame frameWithImageNamed:@"menu1.png"]
                              highlightedSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"menu2.png"]
                                 disabledSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"menu2.png"]];
    [backButton setScale:0.25];
    backButton.positionType = CCPositionTypeNormalized;
    backButton.position = ccp(0.93f, 0.95f); // Top Right of screen
    [backButton setTarget:self selector:@selector(onBackClicked:)];
    [self addChild:backButton];
    
    soundButton = [CCButton buttonWithTitle:@""
                                spriteFrame:[CCSpriteFrame frameWithImageNamed:@"sound-icon.png"]];
    [soundButton setScale:0.25];
    soundButton.positionType = CCPositionTypeNormalized;
    soundButton.position = ccp(0.83f, 0.95f); // Top Right of screen
    [soundButton setTarget:self selector:@selector(muteSoundEffects:)];
    soundButton.cascadeOpacityEnabled = YES;
    [self addChild:soundButton];
    
    musicButton = [CCButton buttonWithTitle:@""
                                          spriteFrame:[CCSpriteFrame frameWithImageNamed:@"music-icon.png"]];
    [musicButton setScale:0.25];
    musicButton.positionType = CCPositionTypeNormalized;
    musicButton.position = ccp(0.77f, 0.95f); // Top Right of screen
    [musicButton setTarget:self selector:@selector(muteMusic:)];
    musicButton.cascadeOpacityEnabled = YES;
    [self addChild:musicButton];
    
    if([OALSimpleAudio sharedInstance].effectsMuted){
        soundButton.opacity = 0.3;
    }
    
    if([OALSimpleAudio sharedInstance].bgMuted){
        musicButton.opacity = 0.3;
    }
    
}

- (void)openLevel1:(id)sender {
    
    [[CCDirector sharedDirector] pushScene:[GameLevel sceneWithLevel:1] ];
        withTransition:[CCTransition transitionRevealWithDirection:CCTransitionDirectionUp duration:3.0f];
}

- (void)openLevel2:(id)sender {
    
    [[CCDirector sharedDirector] pushScene:[GameLevel sceneWithLevel:2] ];
withTransition:[CCTransition transitionPushWithDirection:CCTransitionDirectionLeft duration:1.0f];
}

- (void)openLevel3:(id)sender {
    
    [[CCDirector sharedDirector] pushScene:[GameLevel sceneWithLevel:3] ];
withTransition:[CCTransition transitionPushWithDirection:CCTransitionDirectionLeft duration:1.0f];
}

- (void)openLevel4:(id)sender {
    
    [[CCDirector sharedDirector] pushScene:[GameLevel sceneWithLevel:4] ];
withTransition:[CCTransition transitionPushWithDirection:CCTransitionDirectionLeft duration:1.0f];
}

- (void)openLevel5:(id)sender {
    
    [[CCDirector sharedDirector] pushScene:[GameLevel sceneWithLevel:5] ];
withTransition:[CCTransition transitionPushWithDirection:CCTransitionDirectionLeft duration:1.0f];
}

- (void)onBackClicked:(id)sender
{
    // back to intro scene with transition
    [[CCDirector sharedDirector] popSceneWithTransition:[CCTransition transitionFadeWithDuration:1.0f]];
}

- (void)muteSoundEffects:(id)sender{
    [OALSimpleAudio sharedInstance].effectsMuted = ![OALSimpleAudio sharedInstance].effectsMuted;
    
    if([OALSimpleAudio sharedInstance].effectsMuted){
        [soundButton setOpacity:0.3];
    }
}

- (void)muteMusic:(id)sender{
    [OALSimpleAudio sharedInstance].bgMuted = ![OALSimpleAudio sharedInstance].bgMuted;
    
    if([OALSimpleAudio sharedInstance].bgMuted){
        [musicButton setOpacity:0.3];
    }
}

@end
