//
//  Slide.m
//  mara
//
//  Created by Natanael Silva on 31/03/14.
//  Copyright (c) 2014 Lambu. All rights reserved.
//

#import "Slide.h"

@implementation Slide


- (id) initWithSprite: (CCSprite *) image caption: (NSString *) text {
    
    self.image = image;
    self.caption = text;
    
    
    return self;
}


@end
