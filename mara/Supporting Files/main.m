//
//  main.m
//  mara
//
//  Created by Natanael Silva on 22/01/14.
//  Copyright Lambu 2014. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil, @"AppDelegate");
        return retVal;
    }
}
